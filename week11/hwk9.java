// A program that displays the number of consonants in the first string and the number of vowels (a,e,i,o,u,y) in the 2nd string. 
// Inputs: String 1, first string to evaluate 
//				 String 2, second string to evaluate
// Outputs: Both strings in alphabetical order with first character capitalized and respective consonants or vowel count.
// Written by: Edgar Saavedra
// Last Date Modified: 04/02/14

import java.util.Scanner;

public class hwk9
{

	public static void main(String[] args) 
	{

		Scanner keyboard = new Scanner(System.in);
		System.out.print("\nEnter your first String: ");
		String string1 = keyboard.nextLine();

		System.out.print("Enter your Second String: ");
		String string2 = keyboard.nextLine();

		

		if(string1.compareToIgnoreCase(string2)<0)
		{
			System.out.println("\n"+numberConsonant(string1));
			System.out.println(numberVowels(string2)+"\n");
		}
		else{
			System.out.println("\n"+numberVowels(string2));
			System.out.println(numberConsonant(string1)+"\n");
		}

	}

	/**
		A method called numberVowels to determine the number of vowels in a string.
		@param x string to be evaluated.
	*/

	public static String numberVowels(String x)
	{
		String stringEval = x.toUpperCase();
		int numberVowels = 0;
		for (int i=0; i < stringEval.length() ; i++ ) {
			switch(stringEval.charAt(i)){
				case 'A':
				case 'E':
				case 'I':
				case 'O':
				case 'U':
				case 'Y':
				numberVowels ++;
				break;
			}
		}
		return stringCapitalize(x)+" has "+numberVowels+" vowels";
	}

	/**
		A method called stringCapitalize to capitalize first letter in a string.
		@param x string to be evaluated.
	*/

	public static String stringCapitalize(String x)
	{
		String stringUpper = x.toUpperCase();
		String stringLower = x.toLowerCase();
		return stringUpper.charAt(0)+""+stringLower.substring(1);
	}

	/**
		A method called numberConsonant to determine the number of consonants in a string.
		@param x string to be evaluated.
	*/

	public static String numberConsonant(String x)
	{
		String stringEval = x.toUpperCase();
		int numberConsonants = 0;

		for (int i=0; i < stringEval.length() ; i++ ) {
			switch(stringEval.charAt(i)){
				case 'B':
				case 'C':
				case 'D':
				case 'F':
				case 'G':
				case 'H':
				case 'J':
				case 'K':
				case 'L':
				case 'M':
				case 'N':
				case 'P':
				case 'Q':
				case 'R':
				case 'S':
				case 'T':
				case 'V':
				case 'W':
				case 'X':
				case 'Z':
				numberConsonants++;
				break;
			}
		}

		return stringCapitalize(x)+" has "+numberConsonants+" consonants";

	}	

	// END CLASS 
}