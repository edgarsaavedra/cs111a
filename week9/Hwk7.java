// This program calculates butterfly population estimates
//   Inputs  : males,   estimated number of male butterflies
//             females, estimated number of female butterflies
//   Outputs : total butterflies, sex ratio, variance 
//   Written by: Charlie
//   Modified: March xx, 20xx 
//

import java.util.Scanner;
import java.text.DecimalFormat;

public class Hwk7 {
  public static void main (String[] args) {
    int males, females;
    int totalButterflies, sexRatio, ratioVariance;

    //Declare two new variables to  hold our difference and multiplication values
    int genderDifference, matingPairs;

    Scanner stdin = new Scanner(System.in);

    System.out.println("\n==============================================");
    System.out.println("Butterfly Estimator");
    System.out.println("==============================================\n");
    System.out.print("Enter the estimated males population: ");
    males = stdin.nextInt();
    System.out.print("Enter the estimated females population: ");
    females = stdin.nextInt();

    totalButterflies = males + females; 
    sexRatio         = males / females;
    ratioVariance    = males % females;

    genderDifference = males - females;
    matingPairs      =  males * females;
    
    // 1) Include a new calculation called "Growth factor" by taking the square root of the product (mating pairs). 
    // Use DecimalFormat  to make the output of the growth factor be 3 decimal digits.
    DecimalFormat three_decimal_digits = new DecimalFormat("#0.000");
    double growth_factor = Math.sqrt(matingPairs);

    // 2) Include a new calculation called "Ratio factor" by dividing the growth factor by the sex ratio, unless the sex ratio is zero. 
    // If the sex ratio is zero then set the ratio factor to the square root of the ratio variance. 
    // Use DecimalFormat (discussed last week) to make this output be 1 decimal digit.
    DecimalFormat one_decimal_digit = new DecimalFormat("#0.0");
    double ratio_factor;

    if(sexRatio > 0){
        ratio_factor = growth_factor / sexRatio;
    }else{
        ratio_factor = Math.sqrt(ratioVariance);
    }

    // 3) Include a new calculation called the "Potential population" by raising 2 to the power of the ratio factor, after truncating the ratio factor to an integer. 
    // Use the pow method with 2 as the first argument, and casting the second argment to do this. 
    // Display the potential population as an integer with no decimal digits.
    int n = (int)ratio_factor;

    int potential_population = (int)Math.pow(2, n);


    System.out.println("\nTotal Butterflies      : " + totalButterflies );
    System.out.println("Sex Ratio              : " + sexRatio );
    System.out.println("Variance               : " + ratioVariance );

    System.out.println("Gender Difference      : "+genderDifference);
    System.out.println("Maiting Pairs          : "+matingPairs);

    // New Potential Population
    System.out.println("\nGrowth Factor          : "+three_decimal_digits.format(growth_factor));
    System.out.println("Ratio Factor           : "+one_decimal_digit.format(ratio_factor));
    System.out.println("Potential Population   : "+potential_population+"\n"); 
  }
}
