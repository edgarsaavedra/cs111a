import java.util.Scanner;

/**
	A Simple Program Using A Sentinel To Evalute Series Of Exam Scores
*/

public class Hwk6
{
	public static void main(String[] args) {
		int totalTests;

		Scanner keyboard = new Scanner(System.in);

		System.out.println("-------------------------------------------------------------");
		System.out.println("This Program Evaluates Series Of Exam Scores And Their Totals");
		System.out.println("-------------------------------------------------------------");

		System.out.print("\nEnter A Number Of Exams To Evaluate  Or -1 To End The Program: ");
		totalTests = keyboard.nextInt();

		while (totalTests != -1)
		{
				System.out.print("Enter Score For Exam #1: ");

				int testScoreTotal = keyboard.nextInt();

				int min =testScoreTotal, max = testScoreTotal;

				for(int i = 2; i<=totalTests; i++)
				{
					System.out.print("Enter Score For Exam #"+i+": ");
					testScoreTotal = keyboard.nextInt();
					while(testScoreTotal<min){
						min = testScoreTotal;
					}
					while(testScoreTotal>max){
						max =testScoreTotal;
					}
				}
				System.out.println("\nThe Total Number Of Exams Scores Entered Is: "+totalTests);
				System.out.println("The Largest Exam Score Is: "+max);
				System.out.println("The Smallest Exam Score Is: "+min);
				System.out.println("-------------------------------------------------------------");

			System.out.print("\nWould You like To Evaluate Another Series Of Exams?\nEnter A Number Of Exams To Evaluate Or -1 To End The Program: ");
			totalTests = keyboard.nextInt();
		}
	}
}