Script started on Mon Mar  3 23:48:48 2014
[?1034hbash-3.2$ cat Hwk6.java

import java.util.Scanner;

/**
	A Simple Program Using A Sentinel To Evalute Series Of Exam Scores
*/

public class Hwk6
{
	public static void main(String[] args) {
		int totalTests;

		Scanner keyboard = new Scanner(System.in);

		System.out.println("-------------------------------------------------------------");
		System.out.println("This Program Evaluates Series Of Exam Scores And Their Totals");
		System.out.println("-------------------------------------------------------------");

		System.out.print("\nEnter A Number Of Exams To Evaluate  Or -1 To End The Program: ");
		totalTests = keyboard.nextInt();

		while (totalTests != -1)
		{
			System.out.print("Enter Score For Exam #1: ");

			int testScoreTotal = keyboard.nextInt();

			int min =testScoreTotal, max = testScoreTotal;

			for(int i = 2; i<=totalTests; i++)
			{
				System.out.print("Enter Score For Exam #"+i+": ");
				testScoreTotal = keyboard.nextInt();

				while(testScoreTotal<min){
					min = testScoreTotal;
				}

				while(testScoreTotal>max){
					max =testScoreTotal;
				}
				
			}

			System.out.println("\nThe Total Number Of Exams Scores Entered Is: "+totalTests);
			System.out.println("The Largest Exam Score Is: "+max);
			System.out.println("The Smallest Exam Score Is: "+min);
			System.out.println("-------------------------------------------------------------");

			System.out.print("\nWould You like To Evaluate Another Series Of Exams?\nEnter A Number Of Exams To Evaluate Or -1 To End The Program: ");
			totalTests = keyboard.nextInt();
		}
	}
}

bash-3.2$ java Hwk6
-------------------------------------------------------------
This Program Evaluates Series Of Exam Scores And Their Totals
-------------------------------------------------------------

Enter A Number Of Exams To Evaluate  Or -1 To End The Program: 4
Enter Score For Exam #1: 85
Enter Score For Exam #2: 95
Enter Score For Exam #3: 65
Enter Score For Exam #4: -1

The Total Number Of Exams Scores Entered Is: 4
The Largest Exam Score Is: 95
The Smallest Exam Score Is: -1
-------------------------------------------------------------

Would You like To Evaluate Another Series Of Exams?
Enter A Number Of Exams To Evaluate Or -1 To End The Program: 2
Enter Score For Exam #1: 85
Enter Score For Exam #2: -1

The Total Number Of Exams Scores Entered Is: 2
The Largest Exam Score Is: 85
The Smallest Exam Score Is: -1
-------------------------------------------------------------

Would You like To Evaluate Another Series Of Exams?
Enter A Number Of Exams To Evaluate Or -1 To End The Program: -1

bash-3.2$ exit
exit

Script done on Mon Mar  3 23:49:36 2014
