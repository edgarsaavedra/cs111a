import java.util.Scanner;
import java.text.DecimalFormat;

/**
	A Java program that calculates and displays the parking charges for each customer who parked in the garage yesterday.
	Input the number of hours parked for each customer.
	The program displays the charge for the current customer and calculates and displays the running total of the day's receipts.
*/

public class hwk8
{
	public static void main(String[] args) 
	{

		final int numberCustomers = 3;

		loopReceipts(numberCustomers);

	}

	/**
		A method called loopReceipts to loop through all receipts and display customer charge and
		running total of days receipts.
		@param totalReceipts  
	*/
	public static void loopReceipts(int totalReceipts){
		DecimalFormat dollars = new DecimalFormat("#0.00#");
		int customerHours;
		double customerCost,totalCosts=0.00;
		Scanner keyboard = new Scanner(System.in);

		System.out.println("\n------------------------------------------------------------------");
		System.out.println("This program calculates and displays the parking charges for each "+
											 "\ncustomer who parked in the garage");
		System.out.println("------------------------------------------------------------------");

		for (int i = 0; i<totalReceipts; i++)
		{
				System.out.print("Enter Number Of Hours Customer #"+(i+1)+" Stayed In The Garage: ");
				customerHours = keyboard.nextInt();

				customerCost = calculateCharges(customerHours);

				System.out.println("Customer #"+(i+1)+" charge will be: $"+dollars.format(customerCost)+"\n");
				totalCosts += customerCost;
		}


		System.out.print("-------------------------------------------------------------\n");
		System.out.println("The Running Total of the days receipts is: $"+dollars.format(totalCosts));
		System.out.print("-------------------------------------------------------------\n\n");					
	}

	/**
		A method called calculateCharges to determine the charge for each customer.
		@param numHours  Number of hours customer to be charged for.
	*/

	public static double calculateCharges(int numHours){

		double minFee = 2.00;
		double additionalFeePerHour = 0.50;		
		int hours = numHours;

		if (hours>=1 && hours<=3) {
			return minFee * hours;
		}
		else if (hours>3 && hours<=24) {
			if(hours <= 4){
				return (minFee * hours)+((hours-3)*0.5);
			}
			else{
				return 10.00;
			}
		}
		else{
			return 0.00;
		}
	}

	// END: Program
}