import java.util.Scanner;

/**
	This program takes inputs for number of packges in ints for a software vendor selling packages for $50.
	The Return value gives the discount and total value after discount.
*/

public class Hwk4
{
	public static void main(String[] args)
	{
		double costBeforeDiscount = 50;
		int input;

		Scanner keyboard = new Scanner(System.in);

		System.out.print("Enter the number (in integers) of packages you would like to purchase: ");

		input = keyboard.nextInt();

		if(input < 10){
			System.out.println("You need to purchase at least 10 packages to get a discount");
		}		
		else if(input >= 10 && input <=19){
			System.out.println(input);
			double discount = (costBeforeDiscount * input) * (Double.parseDouble("20")/100);
			double costAfterTaxes = (costBeforeDiscount * input)- discount;
			System.out.println("You get a 20% Discount ($"+discount+") for purchasing "+input+" packages. Your total with discount is: $"+costAfterTaxes);
		}
		else if(input >= 20 && input <=49){
			System.out.println(input);
			double discount = (costBeforeDiscount * input) * (Double.parseDouble("30")/100);
			double costAfterTaxes = (costBeforeDiscount * input)- discount;
			System.out.println("You get a 30% Discount ($"+discount+") for purchasing "+input+" packages. Your total with discount is: $"+costAfterTaxes);
		}
		else if(input >= 50 && input <=99){
			System.out.println(input);
			double discount = (costBeforeDiscount * input) * (Double.parseDouble("40")/100);
			double costAfterTaxes = (costBeforeDiscount * input)- discount;
			System.out.println("You get a 40% Discount ($"+discount+") for purchasing "+input+" packages. Your total with discount is: $"+costAfterTaxes);
		}
		else {
			System.out.println(input);
			double discount = (costBeforeDiscount * input) * (Double.parseDouble("50")/100);
			double costAfterTaxes = (costBeforeDiscount * input)- discount;
			System.out.println("You get a 50% Discount ($"+discount+") for purchasing "+input+" packages. Your total with discount is: $"+costAfterTaxes);
		}
	}
}