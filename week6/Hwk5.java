import java.util.Scanner;

/**
	This program calculates the winner of a sales contest using for loops
	1. Program inputs number of sales persons.
	2. Program inputs number of units sold for each sales person.
	3. Program outputs number of units with largest value


*/

public class Hwk5
{
	public static void main(String[] args) {
		
		Scanner stdin = new Scanner(System.in);

		System.out.print("Enter number of sales people participating in the contest: ");

		int numberSalesPeople = stdin.nextInt();

		if(numberSalesPeople >= 1)
		{
			System.out.print("Enter number of units sold for sales person #1: ");

			int units = stdin.nextInt();

			int min = units, max = units;	

			for(int i = 2; i <= numberSalesPeople; i++)
			{
				System.out.print("Enter number of units sold for sales person #"+i+": ");
				units = stdin.nextInt();
				if(units<min){
					min=units;
				}
				if(units>max){
					max = units;
				}
			}

			System.out.println("Units with the largest value is: "+max);								
		}

		if( numberSalesPeople < 1){
			System.out.println("There needs to be at least 1 sales person in the contest.");
		}
	}
}