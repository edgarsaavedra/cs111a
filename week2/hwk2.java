//**************************************************
// hwk2.java This program displays a simple message.
// Inputs: none
// Outputs: msgs
// Written by : Edgar Saavedra
// Last Modified: Jan 24, 2014
//**************************************************

public class hwk2
{
	public static void main(String[] args)
	{
		System.out.println("Untitled Haiku \tBy Anonymous");

		//first line of output using 2 print statements.
		System.out.print("In the scheme ");
		System.out.print("of things\n");

		//second and third lines of output using a single print statement and \n in the middle. 
		System.out.println("great shading trees know their worth"+"\nto breathe out man\'s breath.\n");

		System.out.println("** End of program **");
	}
}