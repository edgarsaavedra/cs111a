// This program calculates butterfly population estimates
//   Inputs  : males,   estimated number of male butterflies
//             females, estimated number of female butterflies
//   Outputs : total butterflies, sex ratio, variance 
// Written by: Charlie
//   Modified: Jan xx, 20xx 
//
import java.util.Scanner;
public class Hwk3 {
  public static void main (String[] args) {
    int males, females;
    int totalButterflies, sexRatio, ratioVariance;

    //Task 3) 1. Declare two new variables to  hold our difference and multiplication values
    int genderDifference, matingPairs;

    Scanner stdin = new Scanner(System.in);

    System.out.println("\nButterfly Estimator\n");
    System.out.print("Enter the estimated males population: ");
    males = stdin.nextInt();
    System.out.print("Enter the estimated females population: ");
    females = stdin.nextInt();

    totalButterflies = males + females; 
    sexRatio         = males / females;
    ratioVariance    = males % females;

    //Task 3) 2. Do assigment operations to get our values
    genderDifference = males - females;
    matingPairs      =  males * females;
    
    System.out.println("\nTotal Butterflies: " + totalButterflies );
    System.out.println("Sex Ratio        : " + sexRatio );
    System.out.println("Variance         : " + ratioVariance );

    //Task 3) 3. New genderDifference and mattingPairs values printed out
    System.out.println("Gender Difference :"+genderDifference);
    System.out.println("Maiting Pairs    :"+matingPairs);
  }
}
